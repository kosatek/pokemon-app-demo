import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.scss']
})
export class PokemonCardComponent implements OnInit {

  @Input() pokemon = {};
  @Output() goToPokemon: EventEmitter<number>;

  constructor() {
    this.goToPokemon = new EventEmitter<number>();
  }

  ngOnInit() {
  }

  onViewMoreClick(id){
    this.goToPokemon.emit(id);
  }

}
