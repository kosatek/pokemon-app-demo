import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PokemonClientService } from 'src/app/services/pokemon-client.service';

@Component({
  selector: 'app-pokemon-detail-page',
  templateUrl: './pokemon-detail-page.component.html',
  styleUrls: ['./pokemon-detail-page.component.scss']
})
export class PokemonDetailPageComponent implements OnInit {

  pokemon = {};

  constructor(private activeRoute: ActivatedRoute, private pokeClient: PokemonClientService) { 
    const id = this.activeRoute.snapshot.paramMap.get("id");
  }

  async ngOnInit() {
    const id = this.activeRoute.snapshot.paramMap.get("id");
    try{
      this.pokemon = await this.pokeClient.getPokemonById(id);
      console.log(this.pokemon);
    }
    catch(e){
      console.error(e);
    }
  }

}
