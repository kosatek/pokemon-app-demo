import { Component, OnInit } from '@angular/core';
import { PokemonClientService } from 'src/app/services/pokemon-client.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pokemon-list-page',
  templateUrl: './pokemon-list-page.component.html',
  styleUrls: ['./pokemon-list-page.component.scss']
})
export class PokemonListPageComponent implements OnInit {

  pokemonCards = [];

  constructor(private router: Router, private pokeClient: PokemonClientService) { }

  async ngOnInit() {
    try {
      const { results } = await this.pokeClient.getPokemon();
      results.forEach(pokemon => {
        this.pokeClient.getPokemonByUrl(pokemon.url).then(pokemonDetail =>{
          this.pokemonCards.push(pokemonDetail);
        });        
      });
    }
    catch (e) {
      console.error(e);
    }
  }

  onGoToPokemon(id){
    this.router.navigateByUrl(`/pokemon-detail/${id}`);
  }

}
